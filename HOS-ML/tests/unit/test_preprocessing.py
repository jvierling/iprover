import os
from unittest import mock
import subprocess

from preprocessing import run_preprocessor, build_preprocessing_cmd, check_preprocessing_run, grep_output_clauses

def test_prover_options_empty_heuristic():
    heur_path = os.path.dirname(__file__) + "/res/empty_heuristic"
    cmd, _ = build_preprocessing_cmd('problem.p', heur_path, 10)

    res_exp = "./res/iproveropt  --schedule none --preprocessed_out true --tptp_safe_out true --time_out_prep_mult 1.0 --time_out_real 10.00 problem.p"
    assert res_exp in cmd


def test_prover_options_non_empty_heuristic():
    heur_path = os.path.dirname(__file__) + "/res/clausifier_heuristic"
    cmd, _ = build_preprocessing_cmd('problem.p', heur_path, 15)

    res_exp = "./res/iproveropt --clausifier res/vclausify_rel --clausifier_options \"--mode clausify\" --schedule none " \
              + "--preprocessed_out true --tptp_safe_out true --time_out_prep_mult 1.0 --time_out_real 15.00 problem.p"
    assert res_exp in cmd


def test_prover_options_filter():
    # Check that options are filtered correctly, as many flags are set for preprocessing
    heur_path = os.path.dirname(__file__) + "/res/contains_prep_heuristic"
    cmd, _ = build_preprocessing_cmd('problem.p', heur_path, 10)

    res_exp = "./res/iproveropt  --schedule none --preprocessed_out true --tptp_safe_out true --time_out_prep_mult 1.0 --time_out_real 10.00 problem.p"
    assert res_exp in cmd


def test_redirect_to_output_file():
    heur_path = os.path.dirname(__file__) + "/res/empty_heuristic"
    cmd, out_file = build_preprocessing_cmd('problem.p', heur_path, 10)

    # Check that the returned file is in the string and that we have a redirection
    res_exp = "> {0}".format(out_file)
    assert res_exp in cmd


def test_check_preprocessing_run_bad_exit_code(capsys):
    # Need to distinguis between files
    res_path = check_preprocessing_run("out_path", "prob_path", "", -1)

    out, err = capsys.readouterr()
    assert out == ""
    assert err == "ERROR: Preprocessing error with code: -1 and msg: \n"
    assert res_path == "prob_path"


def test_check_preprocessing_run_error_msg(capsys):
    # Need to distinguis between files
    res_path = check_preprocessing_run("out_path", "prob_path", "Test Error", 0)

    out, err = capsys.readouterr()
    assert out == ""
    assert err == "ERROR: Preprocessing error with code: 0 and msg: Test Error\n"
    assert res_path == "prob_path"


def test_check_preprocessing_run_bad_exit_code_and_error_msg(capsys):
    # Need to distinguis between files
    res_path = check_preprocessing_run("out_path", "prob_path", "Test Error", -1)

    out, err = capsys.readouterr()
    assert out == ""
    assert err == "ERROR: Preprocessing error with code: -1 and msg: Test Error\n"
    assert res_path == "prob_path"


def test_check_preprocessing_run_outfile_empty(tmpdir):

    # Create prep file
    prep_content = ""
    prep_out = tmpdir.join('empty_output')
    prep_out.write(prep_content)

    res_path = check_preprocessing_run(str(prep_out), "prob_path", b'', 0)

    # No output in the files so retrieve original problem
    assert res_path == "prob_path"


def test_check_preprocessing_run_outfile_no_cnf(tmpdir):

    # Create prep file
    prep_content = "No clauses here - corrupted output"
    prep_out = tmpdir.join('valid_output')
    prep_out.write(prep_content)

    res_path = check_preprocessing_run(str(prep_out), "prob_path", b'', 0)

    # Corrupted output (no cnf clauses), retrieve original problem
    assert res_path == "prob_path"


def test_check_preprocessing_run_expected_file_and_content(tmpdir):
    # Create prep file
    prep_content = "cnf(1,2,3) \ncnf(3,4,5)\n"
    prep_out = tmpdir.join('valid_output')
    prep_out.write(prep_content)

    res_path = check_preprocessing_run(str(prep_out), "prob_path", b'', 0)

    assert res_path != "prob_path"
    assert res_path == str(prep_out)  # Returned the correct path
    assert prep_out.read() == prep_content  # Can read original path, as it is the same as returned


def test_check_preprocessing_run_filter_prep(tmpdir):

    # Create prep file
    prep_content = "cnf(1,2,3) \ncnf(3,4,5)\nRemove this line"
    expt_content = "cnf(1,2,3) \ncnf(3,4,5)\n"

    prep_out = tmpdir.join('valid_output')
    prep_out.write(prep_content)

    res_path = check_preprocessing_run(str(prep_out), "prob_path", b'', 0)

    # Check that the file and content is correct
    assert res_path == str(prep_out)  # Returned the correct path
    assert prep_out.read() == expt_content  # Correct contents


@mock.patch('preprocessing.grep_output_clauses', return_value="mock_path", auto_spec=True)
def test_check_preprocessing_run_check_filter_grep_call(mock_grep_output_clauses, tmpdir, capsys):

    # Create prep file
    prep_content = "cnf(1,2,3)"
    prep_out = tmpdir.join('valid_output')
    prep_out.write(prep_content)

    res_path = check_preprocessing_run(str(prep_out), "prob_path", b'', 0)

    # Check that grep was called with the correct argument, and that the return value is the mock value
    assert mock_grep_output_clauses.called_once_with(str(prep_out))
    assert res_path == mock_grep_output_clauses.return_value


def test_grep_output_clauses_cnf_only_one_line(tmpdir):

    # Create prep file
    prep_content = "cnf(1,2,3)\n"
    prep_out = tmpdir.join('prep_output')
    prep_out.write(prep_content)

    res_path = grep_output_clauses(str(prep_out))

    assert res_path == prep_out
    assert prep_content == prep_out.read()


def test_grep_output_clauses_cnf_only_multi_line(tmpdir):

    # Create prep file
    prep_content = "cnf(1,2,3)\ncnf(4,5,6)\ncnf(7,8,9)\n"
    prep_out = tmpdir.join('prep_output')
    prep_out.write(prep_content)

    res_path = grep_output_clauses(str(prep_out))

    assert res_path == prep_out
    assert prep_content == prep_out.read()


def test_grep_output_clauses_cnf_and_comments(tmpdir):

    # Create prep file
    prep_content = "cnf(1,2,3)\n%COMMENT\ncnf(4,5,6)\ncnf(7,8,9)\n%COMMENT\n"
    expt_content = "cnf(1,2,3)\ncnf(4,5,6)\ncnf(7,8,9)\n"
    prep_out = tmpdir.join('prep_output')
    prep_out.write(prep_content)

    res_path = grep_output_clauses(str(prep_out))

    assert res_path == prep_out
    assert expt_content == prep_out.read()


def test_run_preprocessor_test_run_process():

    # Stub for an easy process cmd
    cmd = "echo test"
    errs, exc = run_preprocessor(cmd, 3)

    assert errs == b""
    assert exc == 0


def test_run_preprocessor_test_run_process_bad_exitcode_and_error():

    # Stub for an easy process cmd
    cmd = "fail"
    errs, exc = run_preprocessor(cmd, 3)

    assert errs != b""
    assert exc == 127


def test_run_preprocessor_test_run_prep_process():

    # Stub for an easy process cmd
    prob_path = os.path.dirname(__file__) + "/res/problem.p"
    cmd = "./res/iproveropt --schedule none --preprocessed_out true --tptp_safe_out true "
    cmd += " --time_out_prep_mult 1.0 --time_out_real 3.00 {0}".format(prob_path)
    errs, exc = run_preprocessor(cmd, 5)

    assert errs == b""
    assert exc == 0


@mock.patch('subprocess.Popen', auto_spec=True)
def test_run_preprocessor_mock_passing(mock_subprocess):

    # Set mock values
    mock_subprocess.return_value.communicate.side_effect = [("", "first_call"), ("", "second_call")]
    mock_subprocess.return_value.returncode = "0"

    # Simple cmd just to test Mock
    cmd = "echo test"
    errs, exc = run_preprocessor(cmd, 5)

    assert errs == "first_call"
    assert exc == "0"


@mock.patch('subprocess.Popen', auto_spec=True)
def test_run_preprocessor_timeout_expired(mock_subprocess):

    cmd = "echo test"
    mock_subprocess.return_value.communicate.side_effect = [
        subprocess.TimeoutExpired(cmd, 1), ("", "second_call")]
    mock_subprocess.return_value.returncode = 127

    errs, exc = run_preprocessor(cmd, 1)

    # Check that communicate has been called 2 times (exception clause) and check the exit code
    assert errs == "second_call"
    assert exc == -1


def test_run_preprocessor():
    # TODO
    pass


def test_pre_process_problem():
    # TODO
    pass


def test_handle_pre_processing():
    # TODO
    pass
