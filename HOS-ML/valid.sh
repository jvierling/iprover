# Test normal schedule with preprocessing
#python3 prover_driver.py --pre_processor_heuristic heur/default --pre_processor_timeout 5 --schedule fof_schedule test_problems/problem.p 5
#python3 prover_driver.py --pre_processor_heuristic heur/default  --pre_processor_timeout 5 --schedule_mode internal --schedule fof_schedule test_problems/problem.p 5
#python3 prover_driver.py --schedule_mode internal --schedule fof_schedule test_problems/problem.p 5
#python3 prover_driver.py --schedule_mode external --ltb_schedule_low default_claus_test --ltb 1 --ltb_output_dir ../ltb_output ../ltb_test_res/BatchSampleLTBHL4 5


python3 prover_driver.py --schedule_mode external --schedule fof_schedule test_problems/problem.p 5


export TPTP=$(dirname ../ltb_test_res/BatchSampleLTBHL4)
python3 prover_driver.py --schedule_mode external --ltb --ltb_schedule_low_v4 ltb_low_phase_v4  --ltb_schedule_low_v5 ltb_low_phase_v5 --ltb_schedule_high ltb_high_common --ltb_output_dir ../ltb_output ../ltb_test_res/BatchSampleLTBHL4 5


